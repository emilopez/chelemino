# chelemino

Arduino low power SD datalogger. 

## Why chelemino?

In honor of Juan Chelemin (also called Chalimín). More info: https://pueblosoriginarios.com/biografias/chalimin.html

## Hardware Components

- RTC DS3231
- Catalex micro SD
- Arduino Pro Mini (3.3v 8MHz or 5v 16Mhz)

## Library

A library to easily perform the most commons tasks of the datalogger:  get timestamp, save data, send to sleep and wake up

## Details of implementation

Digital Pin 9 enable or disable the power supply to RTC and SD modules. So, you need to configure as OUTPUT and set HIGH or LOW. 

## Example

include <chelemino.h>

datalogger d;

void setup(){
    Serial.begin(9600);
    
}

void loop(){
}    