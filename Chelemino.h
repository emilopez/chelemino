
#ifndef Chelemino_h
#define Chelemino_h

#include "Arduino.h"

class Chelemino{
public:
    Chelemino(uint8_t power_pin, uint8_t cs_sd_pin);
    void wakeUp();
    void deepSleep();
    void initSD();
private:
    uint8_t _power_pin;
    uint8_t _cs_sd_pin;
};

#endif
