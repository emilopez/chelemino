#include "Arduino.h"
#include "Chelemino.h"


Chelemino::Chelemino(uint8_t power_pin, uint8_t cs_sd_pin){
    _power_pin = power_pin; // must be 9
    _cs_sd_pin = cs_sd_pin; // must be 10
}

void Chelemino::wakeUp(){
    pinMode(_power_pin, OUTPUT);
    digitalWrite(_power_pin, HIGH);
    delay(50);
}

void Chelemino::deepSleep(){
    pinMode(_power_pin, OUTPUT);
    digitalWrite(_power_pin, LOW);
    delay(50);
}

void Chelemino::initSD(){
    
}
